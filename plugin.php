<?php
/**
 * Plugin Name: BB Online Payment Calculator Module
 * Description: BB module for beaver builder. Calculator payment  option.
 * Version: 0.0.2
 * Author: Hip Creative, Inc
 * Author URI: http://hip.agency
 */

/*
 * define module directory and url
 */
define('HIP_ONLINE_PAYMENT_CALCULATOR_DIR', plugin_dir_path(__FILE__));
define('HIP_ONLINE_PAYMENT_CALCULATOR_URL', plugins_url('/', __FILE__));
/*
 * register Online Payment Calculator module for beaver builder
 */
function hip_online_payment_calculator_module()
{
	if (class_exists('FLBuilder')) {
		require_once HIP_ONLINE_PAYMENT_CALCULATOR_DIR.'hip-online-payment-calculator-module/HIPOnlinePaymentCalculatorModule.php';
	}
}
add_action('init', 'hip_online_payment_calculator_module');