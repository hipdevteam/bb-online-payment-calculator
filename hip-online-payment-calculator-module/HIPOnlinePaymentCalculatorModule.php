<?php

class HIPOnlinePaymentCalculatorModule extends FLBuilderModule
{
	public function __construct()
	{
		parent::__construct(array(
			'name'            => __('HIP online payment calculator', 'fl-builder'),
			'description'     => __('Online payment calculator option', 'fl-builder'),
			'category'        => __('Hip Modules', 'fl-builder'),
			'dir'             => HIP_ONLINE_PAYMENT_CALCULATOR_DIR . 'hip-online-payment-calculator-module/',
			'url'             => HIP_ONLINE_PAYMENT_CALCULATOR_URL . 'hip-online-payment-calculator-module/',
			'enabled'         => true,
			'editor_export'   => false,
			'partial_refresh' => true
		));
		$this->icon = $this->moduleIcon();
	}
	public function moduleIcon($icon = '')
	{
		return '<svg width="20" height="20" version="1.1" id="Capa_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
	 viewBox="0 0 489.703 489.703" style="enable-background:new 0 0 489.703 489.703;" xml:space="preserve"><g><g id="Icons_28_"><g><path d="M306.755,337.171l-47.131-47.122c-0.337-0.34-0.56-0.716-0.867-1.056H48.164V101.28h312.52v29.07l16.923-16.929
				c7.826-7.834,18.26-12.158,29.334-12.158c0.645,0,1.256,0.154,1.9,0.186v-7.317c0-22.615-18.385-41.029-41.01-41.029H41.011
				C18.408,53.103,0,71.517,0,94.132v202.012c0,22.615,18.408,41.028,41.011,41.028h117.24v51.994h-37.552
				c-9.354,0-16.936,7.583-16.936,16.947v13.547c0,9.364,7.581,16.941,16.936,16.941H288.13c9.337,0,16.917-7.578,16.917-16.941
				v-13.549c0-9.364-7.58-16.947-16.917-16.947h-37.553v-51.993H306.755z"/><path d="M486.971,209.53l-36.731-36.71L319.043,304.057l36.696,36.688c1.815,1.83,4.219,2.746,6.616,2.746
				c2.395,0,4.783-0.916,6.604-2.746L486.971,222.76c1.752-1.747,2.732-4.121,2.732-6.619
				C489.704,213.663,488.723,211.28,486.971,209.53z"/><path d="M400.325,136.124L282.331,254.101c-1.759,1.755-2.753,4.13-2.753,6.627c0,2.48,0.994,4.854,2.753,6.61l14.907,14.917
				l131.231-131.222l-14.926-14.909c-1.805-1.835-4.202-2.746-6.604-2.746C404.542,133.378,402.144,134.289,400.325,136.124z"/></g></g></g></svg>
';
	}
}
/*Builder options*/
FLBuilder::register_module('HIPOnlinePaymentCalculatorModule', array(
	'general' => array(
		'title'    => __('Layout', 'fl-builder'),
		'sections' => array(
			'general' => array(
				'title'  => '',
				'fields' => array(
					'length_of_treatment' => array(
						'type'         => 'select',
						'label'        => __('Length of Treatmaent', 'fl-builder'),
						'default'      => 'include',
						'size' => 100,
						'options'      => array(
							'month' => 'By Month',
							'week' => 'By Week'
						)
					),
					'min_length'  => array(
						'type'        => 'text',
						'label'       => __('Min. Length of Treatment', 'fl-builder'),
						'size'        => 6,
					),
					'max_length'  => array(
						'type'        => 'text',
						'label'       => __('Max. Length of Treatment', 'fl-builder'),
						'size'        =>30,
					),
					'interval_length'  => array(
						'type'         => 'text',
						'label'        => __('Interval for Length of Treatment', 'fl-builder'),
						'size'        =>6,
					),

				)
			)
		)
	),
	'style'   => array(
		'title'    => __('Style', 'fl-builder'),
		'sections' => array(
			'general'       => array(
				'title'  => __('Background', 'fl-builder'),
				'fields' => array(
					'bg_color' => array(
						'type'       => 'color',
						'label'      => __('Background Color', 'fl-builder'),

					),
				)
			),
			'border_style' => array(
				'title'  => __('Border Color', 'fl-builder'),
				'fields' => array(
					'border_color'      => array(
						'type'       => 'color',
						'label'      => __('Color', 'fl-builder')
					),
				)
			),
			'text_style' => array(
				'title'  => __('Text Color', 'fl-builder'),
				'fields' => array(
					'text_color'      => array(
						'type'       => 'color',
						'label'      => __('Color', 'fl-builder')
					),
				)
			),
			'rang_style' => array(
				'title'  => __('Rang Background Color', 'fl-builder'),
				'fields' => array(
					'range_color'      => array(
						'type'       => 'color',
						'label'      => __('Color', 'fl-builder')
					),
				)
			),
			'rang_pointer_style' => array(
				'title'  => __('Rang Pointer Color', 'fl-builder'),
				'fields' => array(
					'pointer_color'      => array(
						'type'       => 'color',
						'label'      => __('Color', 'fl-builder')
					),
				)
			),
			'rang_line_style' => array(
				'title'  => __('Rang line Color', 'fl-builder'),
				'fields' => array(
					'line_color'      => array(
						'type'       => 'color',
						'label'      => __('Color', 'fl-builder')
					),
				)
			),
		)
	),
));
