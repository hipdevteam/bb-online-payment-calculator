/**Rage front end css**/

<?php if(!empty($settings->bg_color) ) : ?>

.fl-node-<?php echo $id; ?> .hip-online-payment-calculator-wrapper .hip-payment-calculator-inner,
.hip-online-payment-calculator-wrapper .hip-online-payment-calculator-result {
	background: <?php  echo '#'.$settings->bg_color;?>;
}

<?php endif ?>

<?php if(!empty($settings->border_color)) : ?>

.fl-node-<?php echo $id; ?> .hip-online-payment-calculator-wrapper .hip-payment-calculator-inner{
	border-color: <?php  echo '#'.$settings->border_color;?>;
}

<?php endif ?>

<?php if(!empty($settings->range_color)) : ?>

.fl-node-<?php echo $id; ?> .hip-online-payment-calculator-wrapper .hip-payment-calculator-inner .hip-online-payment-calculator-slider .hip-online-payment-calculator-slider-rang .rang{
	background: <?php  echo '#'.$settings->range_color;?>;
}
<?php endif ?>

<?php if(!empty($settings->pointer_color)) : ?>

.fl-node-<?php echo $id; ?> .hip-online-payment-calculator-wrapper .hip-payment-calculator-inner .hip-online-payment-calculator-slider .hip-online-payment-calculator-slider-rang .rang::-webkit-slider-thumb{
	background: <?php  echo '#'.$settings->pointer_color;?>;
}

<?php endif ?>

<?php if(!empty($settings->line_color)) : ?>

.fl-node-<?php echo $id; ?> .hip-online-payment-calculator-wrapper .hip-payment-calculator-inner .hip-online-payment-calculator-slider .hip-online-payment-calculator-range-label span:after {

	background-color: <?php  echo '#'.$settings->line_color;?>;
}

<?php endif ?>

<?php if(!empty($settings->text_color)) : ?>

.fl-node-<?php echo $id; ?> .hip-online-payment-calculator-wrapper .hip-payment-calculator-inner .hip-payment-calculator-fields .hip-payment-filed label,
.hip-online-payment-calculator-wrapper .hip-payment-calculator-inner .hip-online-payment-calculator-slider .hip-online-payment-calculator-slider-text p,
.hip-online-payment-calculator-wrapper .hip-payment-calculator-inner .hip-online-payment-calculator-slider .hip-online-payment-calculator-range-label span,
.hip-online-payment-calculator-wrapper .hip-online-payment-calculator-result .hip-online-payment-result-text{

	color: <?php  echo '#'.$settings->text_color;?>;
}

<?php endif ?>

