<?php
$args = array();

if(!empty( $settings->length_of_treatment ) ){

	if ($settings->length_of_treatment === 'month') {
		$args['month'] = 'Length of Treatment (months)';
	}
	if ($settings->length_of_treatment === 'week') {
		$args['week'] = 'Length of Treatment (week)';
	}
}
$min_length = !empty($settings->min_length) ? $settings->min_length : 0;
$max_length = !empty($settings->max_length) ? $settings->max_length : 0;
$interval_length = !empty($settings->interval_length) ? $settings->interval_length : 0;

$calculation = array();


?>
<div class="hip-online-payment-calculator-wrapper">
	<div class="hip-payment-calculator-inner">
		<div class="hip-payment-calculator-fields">
			<div class="hip-payment-filed">
				<label for="">Treatment Cost</label>
				<input type="text" class="treatment-cost">
				<div class="field-error"></div>
			</div>
			<div class="hip-payment-filed">
				<label for="">Down Payment</label>
				<input type="text" class="down-payment">
				<div class="field-error"></div>
			</div>
			<div class="hip-payment-filed">
				<label for="">Insurance Coverage</label>
				<input type="text" class="insurance-coverage">
				<div class="field-error"></div>
			</div>
		</div>
		<div class="hip-online-payment-calculator-slider">
			<div class="hip-online-payment-calculator-slider-text">
				<p>
					<?php
					 if (isset($settings->length_of_treatment)){
					 	foreach ($args as  $value ){
					 		echo $value;
						}
					 }
					?>
				</p>
			</div>
			<div class="hip-online-payment-calculator-range-label">
				<?php if(($min_length > 0) && ($max_length > 0)&& ($interval_length > 0)):?>
				<?php
					$i = $min_length;
					while($i<=$max_length):
				?>
				<span class="hip-online-payment-calculator-range-label-value"><?php echo $i;?></span>
				<?php  $i = $i + $interval_length; ?>
				<?php  endwhile; endif;?>
			</div>
			<div class="hip-online-payment-calculator-slider-rang">
				<input  type="range" min="<?php echo (int)$min_length ;?>" max="<?php echo (int)$max_length ;?>" value="<?php echo (int)$min_length; ?>" class="rang slideRange" id="slideRange" step="<?php echo (int) $interval_length;?>" data-treatment-length-type="<?php echo $settings->length_of_treatment?>">
			</div>
		</div>

	</div>
	<div class="hip-online-payment-calculator-result">
		<span class="hip-online-payment-result-text">Your Cost Is: </span>
		<span id="payment_results"></span>
	</div>
</div>

