(function ($) {

	$('#slideRange, .hip-payment-filed input[type="text"]').on('input', function () {
		var slider = $('#slideRange'),
			treatmentLength = slider.val(),
			treatmentLengthType = slider.data('treatment-length-type'),
			treatmentCost = $('.treatment-cost').val(),
			downPayment = $('.down-payment').val(),
			insurace = $('.insurance-coverage').val(),
			isvalid = true;

		$('input[type="text"]').on("keypress keyup blur",function (event) {
			$(this).val($(this).val().replace(/[^0-9\.\$]/g,''));

			if (event.keyCode === 46 && this.value.split('.').length === 2) {
				event.preventDefault();
			}
		});

		if (!treatmentCost) {
			$('.treatment-cost').parent().find('.field-error').html('Field cannot be empty!');
			isvalid = false;
		} else {
			$('.treatment-cost').parent().find('.field-error').html('');
		}
		if (!downPayment ) {
			$('.down-payment').parent().find('.field-error').html('Field cannot be empty!');
			isvalid = false;
		} else {
			$('.down-payment').parent().find('.field-error').html('');
		}
		if (!insurace) {
			$('.insurance-coverage').parent().find('.field-error').html('Field cannot be empty!');
			isvalid = false;
		} else {
			$('.insurance-coverage').parent().find('.field-error').html('');
		}
		if (isvalid) {
			//$('#payment_results').html('$' + calculateCost(treatmentLength,treatmentLengthType)+'/per month')
			$('#payment_results').html('$' + (calculateCost(treatmentLength,treatmentLengthType)).toFixed(2) + '/per month')
		}

	});

	var calculateCost = function (length,type) {

		var treatmentCost = $('.treatment-cost').val() ? parseFloat(removeDollar($('.treatment-cost').val())) : 0 ,
			downPayment = parseFloat(removeDollar($('.down-payment').val())),
			insurace =  parseFloat(removeDollar($('.insurance-coverage').val()));


		if (type.toString() === 'week') {
			return (treatmentCost - (downPayment + insurace)) / (length * 4);
		}
		return (treatmentCost - (downPayment + insurace)) / length;
	}
	var removeDollar = function (inputValue) {
		 var withoutDoller = inputValue.replace("$", '');
		return withoutDoller;
	}

})(jQuery);